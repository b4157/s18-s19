//[SECTION] Objects

  //Objects -> is a collection of related data and/or functionalities. the purpose of an object is not only to store multiple  data-sets but also to represent real world objects.

  //SYNTAX: 
      // let/const variableName = {
      // 	key/property: value
      // }

      //Note: Information stored in objects are represented in key:value pairing. 

   //lets use this example to describe a real-world item/object
   let cellphone = {
   	 name: 'Nokia 3210',
   	 manufactureDate: '1999',
   	 price: 1000 
   }; //'key' -> is also mostly referred to as a 'property' of an object. 

   console.log(cellphone); 
   console.log(typeof cellphone); 

   //How to store multiple objects, 

   //you can use an array struture to store them.
   let users = [
	   { 
	   	 name: 'Anna', 
	   	 age: '23'
	   },
	   { 
	   	 name: 'Nicole', 
	   	 age: '18'
	   },
	   { 
	   	 name: 'Smith', 
	   	 age: '42'
	   },
	   { 
	   	 name: 'Pam', 
	   	 age: '13'
	   },
	   { 
	   	 name: 'Anderson', 
	   	 age: '26'
	   }
  	];

   console.log(users); 
   //now there is an alternative way when displaying values of an object collection
   console.table(users); 

   //Complex examples of JS objects

   //NOTE: Different data types may be stored in an object's property creating more complex data structures

   //When using JS Objects, you can also insert 'Methods'.

   //Methods -> are useful for creating reusable functions that can perform tasks related to an object. 
   let friend = {
   	 //properties
   	 firstName: 'Joe', //String or numbers
   	 lastName: 'Smith', 
   	 isSingle: true, //boolean
     emails: ['joesmith@gmail.com', 'jsmith@company.com'], //array
     address: { //Objects
     	city: 'Austin',
     	state: 'Texas',
     	country: 'USA'
     },
     //methods 
     introduce: function() {
     	console.log('Hello Meet my new Friend'); 
     },
     advice: function() {
     	console.log('Give friend an advice to move on');
     }, 
     wingman: function() {
     	console.log('Hype friend when meeting new people');
     }
   }
   console.log(friend);

//[SECTION] ALTERNATE APPROACHES on HOW TO DECLARE AN OBJECT IN JAVASCRIPT. 
		
	//1. How to create an Object using a Constructor function?

	// Constructor function -> we will need to create a reusable function in which we will declare a blueprint to describe the anatomy of an object that we wish to create. 

	//to declare properties within a constructor function, you have to be familiar with the 'this' keyword
	function Laptop(name, year, model) {
	   this.brand = year;
	   this.manufacturedOn = name; 
	   this.model = model; 
	}; 

	//'this' -> NOT A VARIABLE but a *keyword*.
	  //this -> refers to the object in this example, 'this' refers to the 'global' object *Laptop* because the this keyword is within the scope of the laptop constructor function. 

	//to create a new object that has the properties stated above, we will use the 'new' keyword for us to be able to create a new instance of an object.

	let laptop1 = new Laptop('Sony', 2008, 'viao');
	let laptop2 = new Laptop('Apple', 2019, 'Mac'); 
	let laptop3 = new Laptop('HP', 2015, 'hp'); 

	//what is i would want to store them in a single container and display them in a tabular format. 

	let gadgets = [laptop1, laptop2, laptop3]; 
	console.table(gadgets); 

	//==> use and benefits: this is useful when creating 'instances' of several object that have the same data structures.

	//instance -> this refers to a concrete occurence of any object which emphasizes on the distinct/unique identity of the object/subject. 

	//example2:

	//lets create an instane of multiple pokemon characters

	//use the 'this' keyword to associate each property with the global object and bind them with the respective values
	function Pokemon(name, type, level, trainer) {
		//properties
		this.pokemonName = name; 
		this.pokemonType = type; 
		this.pokemonHealth = 2 * level;  
		this.pokemonLevel = level; 
		this.owner = trainer; 
		//methods
		this.tackle = function(target){
			console.log(this.pokemonName + ' tackled ' + target.pokemonName); 
		};
		this.greetings = function() {
			console.log(this.pokemonName + ' says Hello! '); 
		};
	}

	//create instance of a pokemon using the constructor function.
	let pikachu = new Pokemon('Pikachu', 'electric', 3, 'Ash ketchup');
	let ratata = new Pokemon('Ratata', 'normal', 4, 'Misty');
    let snorlax = new Pokemon('Snorlax', 'normal', 5, 'Gary Oak'); 

    //display all pokemons in the console in a table format
    let pokemons = [pikachu, ratata, snorlax]; 

    console.table(pokemons); 

//[SECTION] How to access Object Properties?
   
   //using (.) dot notation you can access properties of an object

   //SYNTAX: objectName.propertyName.
   console.log(snorlax.owner); 
   console.log(pikachu.pokemonType); 
   //practice acessing a method of an object. 
   pikachu.tackle(ratata); 
   snorlax.greetings(); 


   //[ALTERNATE APPROACH]
   //[] square brackets as an alternate approach.
   //SYNTAX: objectName['property name']
   console.log(ratata['owner']); 
   console.log(ratata['pokemonLevel']); //IT WILL return undefined, if you try to access a property that does NOT exists. 


   //Which is the BEST USE CASE 'dot notation' OR square bracket? 

   // we will stick with dot notations when accessing properties of an object. as our convention when managing propertoes of an an object.

   //we will use square brackets when dealing with indexes of an array. 


//[ADDITIONAL KNOWLEDGE]:

   //KEEP this in mind, you can write and declare objects this way:

   //to create an objects, you will use object literals '{}'

   let trainer = {}; //empty object
   console.log(trainer); 

   //will i be able to add properties from outside an object? YES
   trainer.name = 'Ash Ketchum'; 
   console.log(trainer); 
   trainer.friends = ['Misty', 'Brock', 'Tracey']; 
   console.log(trainer); //array

   //method
   trainer.speak = function() {
   	console.log('Pikachu, i choose you!');
   }
   
   trainer.speak(); 
   //possible approach
